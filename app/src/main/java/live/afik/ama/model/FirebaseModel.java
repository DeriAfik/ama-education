package live.afik.ama.model;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

import live.afik.ama.MainActivity;
import live.afik.ama.lesson.MyMessage;

import static live.afik.ama.MainActivity.TAG;


public class FirebaseModel {

    public static final int RC_SIGN_IN = 1;
    FirebaseAuth mAuth;

    public static FirebaseModel instance = new FirebaseModel();

    private FirebaseModel() {}

    public void addMessageToLesson(String message, Object lessonId) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = database.getReference("messages/" + lessonId);

        MyMessage m = new MyMessage();
        m.setMessage(message);
        m.setSender(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());

        mRef.child(mRef.push().getKey()).setValue(m);
    }

    public FirebaseRecyclerOptions<MyMessage> getMessagesForLesson(Object lessonId) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = database.getReference("messages/" + lessonId);

        FirebaseRecyclerOptions<MyMessage> options = new FirebaseRecyclerOptions.Builder<MyMessage>()
                .setQuery(mRef.limitToFirst(50), MyMessage.class)
                .build();

        return options;
    }

    public void loginIfNotAuthenticated(Activity a) {

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() == null)
        {

            a.startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.EmailBuilder().build(),
                                    new AuthUI.IdpConfig.GoogleBuilder().build()))
                            .build(),
                    RC_SIGN_IN);

        } else {
            Log.w(TAG, "The user: " + mAuth.getCurrentUser().getDisplayName() + " is already connected");
            initialiseOnlinePresence();
        }
    }

    public static void initialiseOnlinePresence() {

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myConnectionsRef = database.getReference("users/" + userId + "/connections");

        final DatabaseReference lastOnlineRef = database.getReference("/users/" + userId + "/lastOnline");

        final DatabaseReference connectedRef = database.getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    DatabaseReference con = myConnectionsRef.push();
                    con.onDisconnect().removeValue();
                    lastOnlineRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
                    con.setValue(Boolean.TRUE);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled at .info/connected");
            }
        });
    }

}
