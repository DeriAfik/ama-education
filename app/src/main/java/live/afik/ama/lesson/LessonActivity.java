package live.afik.ama.lesson;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.util.Arrays;
import java.util.HashMap;

import live.afik.ama.MainActivity;
import live.afik.ama.R;
import live.afik.ama.model.FirebaseModel;

public class LessonActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 1;
    Lesson lesson;
    MessageAdapter adapter;
    RecyclerView lstMessages;
    ImageView imageview;
    TextView tvTitle;
    TextView tvMentor;
    TextView tvUserStatus;
    FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);

        FirebaseModel.instance.loginIfNotAuthenticated(LessonActivity.this);
        mAuth = FirebaseAuth.getInstance();

        imageview = (ImageView) findViewById(R.id.lesson_cover);
        lstMessages = (RecyclerView) findViewById(R.id.lstMessages);
        tvTitle = findViewById(R.id.lesson_title);
        tvMentor = findViewById(R.id.tvMentor);
        tvUserStatus = findViewById(R.id.tv_user_status);

        DatabaseReference lessonRef = FirebaseDatabase.getInstance().getReference("lessons/" + getIntent().getExtras().get("lesson"));

        adapter = new MessageAdapter( FirebaseModel.instance.getMessagesForLesson(getIntent().getExtras().get("lesson")), mAuth );

        lstMessages.setLayoutManager(new LinearLayoutManager(this));
        lstMessages.setAdapter(adapter);

        lstMessages.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                lstMessages.scrollToPosition(adapter.getItemCount() - 1);
            }
        });

        Button btnSend = (Button) findViewById(R.id.btnSend);
        final EditText etMessage = (EditText) findViewById(R.id.etMessage);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseModel.instance.addMessageToLesson(etMessage.getText().toString(), getIntent().getExtras().get("lesson"));

                etMessage.setText("");

                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }
        });

        // Read from the database
        lessonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lesson = new Lesson();
                lesson.expertise = (String) dataSnapshot.child("expertise").getValue();
                lesson.name = (String) dataSnapshot.child("name").getValue();
                lesson.image = (String) dataSnapshot.child("image").getValue();
                lesson.username = (String) dataSnapshot.child("username").getValue();
                lesson.id = dataSnapshot.getKey();
                lesson.userId = (String) dataSnapshot.child("userId").getValue();

                loadLesson();
                setUserStatus();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(MainActivity.TAG, "Failed to read value.", error.toException());
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    private void loadLesson() {
        tvTitle.setText(lesson.name);
        tvMentor.setText(lesson.username);
        loadImage();
    }

    private void loadImage() {
        final FirebaseStorage storage = FirebaseStorage.getInstance();

        Uri imageUri = Uri.parse(lesson.image);
        Glide.with(LessonActivity.this)
                .load(imageUri)
                .into(imageview);
    }

    private void setUserStatus() {
        if (lesson.userId.equals(mAuth.getCurrentUser().getUid())) {
            tvUserStatus.setText(R.string.your_lesson);
            return;
        }

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference userConnectionRef = database.getReference("users/" + lesson.userId + "/connections");

        final String offlineState = lesson.username + " is offline";
        final String onlineState = lesson.username + " is online";

        tvUserStatus.setText(offlineState);

        userConnectionRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                Log.d(MainActivity.TAG, "DataSnapshot:" + dataSnapshot + " Child Count: " + dataSnapshot.getChildrenCount());

                if (dataSnapshot.getChildrenCount() > 0) {
                    tvUserStatus.setText(onlineState);
                    tvUserStatus.setTextColor(Color.GREEN);
                } else {
                    tvUserStatus.setText(offlineState);
                    tvUserStatus.setTextColor(Color.RED);
                }
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Log.w(MainActivity.TAG, "DatabaseError:" + databaseError);
            }
        });
    }
}
