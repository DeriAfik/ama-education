package live.afik.ama.lesson;

import android.graphics.Bitmap;

public class Lesson {
    public String id;
    public String name;
    public String expertise;
    public String image;
    public String userId;
    public String username;
}
