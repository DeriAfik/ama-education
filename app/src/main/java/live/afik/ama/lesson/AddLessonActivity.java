package live.afik.ama.lesson;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import live.afik.ama.R;
import live.afik.ama.feed.FeedActivity;

public class AddLessonActivity extends AppCompatActivity {

    public static final int PICK_IMAGE = 1;
    CharSequence expertise[] = new CharSequence[] {"Linear Algebra", "Discrete Math", "Computer Science", "Calculus"};
    Lesson lesson;
    ImageView imageview;
    String imageName;
    EditText etName;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lesson);

        lesson = new Lesson();

        Button expertisePicker = findViewById(R.id.expertise_picker);
        Button imgPicker = findViewById(R.id.image_picker);
        Button addBtn = findViewById(R.id.add_btn);
        final ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        etName = findViewById(R.id.etLesson);
        final TextView tvExpertise = findViewById(R.id.tvExpertise);

        etName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }
        });

        expertisePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(AddLessonActivity.this);
                builder.setTitle("Pick your expertise");
                builder.setItems(expertise, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        lesson.expertise = expertise[which].toString();
                        tvExpertise.setText(expertise[which]);
                    }
                });
                builder.show();
            }
        });

        imgPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, PICK_IMAGE);

            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lesson.expertise = tvExpertise.getText().toString();
                lesson.name = etName.getText().toString();

                if (lesson.expertise != "" || lesson.name != "") {
                    progressBar.setVisibility(View.VISIBLE);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();

                    // Create a storage reference from our app
                    StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                    final StorageReference imagesRef = storageRef.child("lessons/images/" + imageName);

                    imagesRef.putBytes(data).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return imagesRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                DatabaseReference lessonsRef = FirebaseDatabase.getInstance().getReference("lessons");
                                progressBar.setVisibility(View.INVISIBLE);
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                Uri downloadUri = task.getResult();
                                lesson.image = downloadUri.toString();
                                lesson.username = user.getDisplayName();
                                lesson.userId = user.getUid();

                                lessonsRef.child(lessonsRef.push().getKey()).setValue(lesson);

                                Intent intent = new Intent(getBaseContext(), FeedActivity.class);
                                startActivity(intent);

                            } else {
                                Toast.makeText(AddLessonActivity.this, "upload failed: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        }
                    });

                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
            imageName = filePath.getLastPathSegment();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageview = (ImageView) findViewById(R.id.cover_img);
                imageview.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
