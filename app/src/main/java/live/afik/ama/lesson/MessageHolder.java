package live.afik.ama.lesson;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import live.afik.ama.R;

/**
 * Created by afikderi on 09/08/2018.
 */

public class MessageHolder extends RecyclerView.ViewHolder {
    public TextView tvSender;
    public TextView tvMessage;
    public MessageHolder(View itemView) {
        super(itemView);
        tvSender = (TextView) itemView.findViewById(R.id.tvSender);
        tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);
    }
}
