package live.afik.ama.lesson;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;

import live.afik.ama.R;


public class MessageAdapter extends FirebaseRecyclerAdapter<MyMessage, MessageHolder> {
    FirebaseAuth mAuth;

    public MessageAdapter(FirebaseRecyclerOptions<MyMessage> lesson, FirebaseAuth mAuth) {
        super(lesson);

        this.mAuth = mAuth;
    }


    @Override
    protected void onBindViewHolder(@NonNull MessageHolder holder, int position, @NonNull MyMessage model) {
        String senderName = model.getSender().equals(mAuth.getCurrentUser().getDisplayName()) ? "You:" : model.getSender() + ":";

        holder.tvSender.setText(senderName);
        holder.tvMessage.setText(model.getMessage());
    }

    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_layout, parent, false);
        return new MessageHolder(v);
    }
}
