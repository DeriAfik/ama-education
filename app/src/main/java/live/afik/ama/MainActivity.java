package live.afik.ama;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import live.afik.ama.feed.FeedActivity;
import live.afik.ama.lesson.AddLessonActivity;
import live.afik.ama.model.FirebaseModel;

import static live.afik.ama.model.FirebaseModel.RC_SIGN_IN;
import static live.afik.ama.model.FirebaseModel.initialiseOnlinePresence;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MY_TAG";
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseModel.instance.loginIfNotAuthenticated(MainActivity.this);

        ImageButton feedBtn = findViewById(R.id.feed_btn);
        ImageButton addLessonBtn = findViewById(R.id.add_new_btn);

        feedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), FeedActivity.class);
                startActivity(intent);
            }
        });

        addLessonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), AddLessonActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            final ImageButton myLessonsBtb = findViewById(R.id.my_lessons_btn);
            Query userLessons = FirebaseDatabase.getInstance().getReference("lessons").orderByChild("userId").equalTo(mAuth.getUid()).limitToFirst(10);

            userLessons.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getChildrenCount() > 0) {
                        Log.d(TAG, "I have lessons...");
                        myLessonsBtb.setVisibility(View.VISIBLE);

                        myLessonsBtb.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getBaseContext(), FeedActivity.class);
                                intent.putExtra("user_filter", Boolean.TRUE);
                                startActivity(intent);
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK){
            initialiseOnlinePresence();
        }
    }
}
