package live.afik.ama.feed;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import live.afik.ama.MainActivity;
import live.afik.ama.lesson.Lesson;
import live.afik.ama.R;

public class FeedActivity extends AppCompatActivity implements View.OnClickListener {

    ListView listView;
    List<Lesson> lessons;
    FeedAdapter adapter;
    FirebaseAuth mAuth;
    AlphaAnimation inAnimation;
    AlphaAnimation outAnimation;
    FrameLayout progressBarHolder;
    Button ShowAllFilter;
    Button[] filters;
    DatabaseReference lessonsRef;
    Boolean filtersSetup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        progressBarHolder = (FrameLayout) findViewById(R.id.progressBarHolder);
        listView = findViewById(R.id.lst);
        ShowAllFilter = findViewById(R.id.filter_1);
        filters = new Button[]{findViewById(R.id.filter_2), findViewById(R.id.filter_3), findViewById(R.id.filter_4)};

        mAuth = FirebaseAuth.getInstance();

        lessons = new LinkedList<>();

        adapter = new FeedAdapter(this , lessons);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        lessonsRef = database.getReference("lessons");

        try {
            Boolean filterByUser = (Boolean) getIntent().getExtras().get("user_filter");
            if (filterByUser) {
                Query filteredLessonsRef = lessonsRef.orderByChild("userId").equalTo(mAuth.getUid());
                filteredLessonsRef.addValueEventListener(new LessonsBuilder());
            } else {
                lessonsRef.addValueEventListener(new LessonsBuilder());
            }
        } catch (NullPointerException e) {
            lessonsRef.addValueEventListener(new LessonsBuilder());
        }

    }

    private void buildLessons(DataSnapshot dataSnapshot) {
        lessons.clear();
        for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {

            Lesson l = new Lesson();
            l.expertise = (String) messageSnapshot.child("expertise").getValue();
            l.name = (String) messageSnapshot.child("name").getValue();
            l.username = (String) messageSnapshot.child("username").getValue();
            l.userId = (String) messageSnapshot.child("userId").getValue();
            l.image = (String) messageSnapshot.child("image").getValue();
            l.id = messageSnapshot.getKey();

            lessons.add(l);
            adapter.notifyDataSetChanged();
        }

        if (!filtersSetup) {
            buildFilters();
            filtersSetup = true;
        }
        stopLoader();
    }

    private void buildFilters() {
        LinkedList<String> filterNames = new LinkedList<>();
        for (Lesson l : lessons) {
            if (!filterNames.contains(l.expertise)) {
                filterNames.push(l.expertise);
            }
        }

        ShowAllFilter.setText(R.string.show_all);
        ShowAllFilter.setOnClickListener(this);

        for (Button b : filters) {
            if (!filterNames.isEmpty()) {
                b.setText(filterNames.pop());
                b.setOnClickListener(this);
            } else {
                ViewGroup layout = (ViewGroup) b.getParent();
                if(null!=layout)
                    layout.removeView(b);
                b.setVisibility(View.INVISIBLE);
            }
        }
    }

    public class LessonsBuilder implements ValueEventListener {

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            Log.d(MainActivity.TAG, "Number of lessons : " + dataSnapshot.getChildrenCount());
            buildLessons(dataSnapshot);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError error) {
            Log.w(MainActivity.TAG, "Failed to read value.", error.toException());
        }
    }

    @Override
    public void onClick(View v) {
        // start progress animation
        startLoader();

        // set filter
        String filterName = ((Button) v).getText().toString();
        if (!filterName.equals(getString(R.string.show_all))) {
            Query filteredLessonsRef = lessonsRef.orderByChild("expertise").equalTo(filterName);
            filteredLessonsRef.addValueEventListener(new LessonsBuilder());
        } else {
            lessonsRef.orderByKey().addValueEventListener(new LessonsBuilder());
        }

        Log.d(MainActivity.TAG, "Filtering by: " + filterName);
    }

    private void startLoader() {
        inAnimation = new AlphaAnimation(0f, 1f);
        inAnimation.setDuration(200);
        progressBarHolder.setAnimation(inAnimation);
        progressBarHolder.setVisibility(View.VISIBLE);
    }

    private void stopLoader() {
        outAnimation = new AlphaAnimation(1f, 0f);
        outAnimation.setDuration(200);
        progressBarHolder.setAnimation(outAnimation);
        progressBarHolder.setVisibility(View.GONE);
    }

}
