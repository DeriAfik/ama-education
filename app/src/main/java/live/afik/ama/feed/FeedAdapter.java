package live.afik.ama.feed;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;

import java.util.List;

import live.afik.ama.lesson.Lesson;
import live.afik.ama.lesson.LessonActivity;
import live.afik.ama.R;

public class FeedAdapter extends BaseAdapter {

    List<Lesson> lessons;
    Context context;

    FeedAdapter(Context context, List<Lesson> lessons) {
        this.context = context;
        this.lessons = lessons;
    }

    @Override
    public int getCount() {
        return lessons.size();
    }

    @Override
    public Object getItem(int i) {
        return lessons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).
                    inflate(R.layout.lesson_layout, viewGroup, false);
        }

        final FirebaseStorage storage = FirebaseStorage.getInstance();

        final Lesson currentItem = (Lesson) getItem(i);

        TextView tvTitle = (TextView) view.findViewById(R.id.title);
        TextView tvSubject = (TextView) view.findViewById(R.id.subject);
        Button helpBtn = (Button) view.findViewById(R.id.help_btn);

        tvTitle.setText(currentItem.name);
        tvSubject.setText(currentItem.expertise);


        final ImageView imageview = (ImageView) view.findViewById(R.id.imageView);

        Uri imageUri = Uri.parse(currentItem.image);
        Glide.with(view.getContext())
                .load(imageUri)
                .into(imageview);

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LessonActivity.class);
                intent.putExtra("lesson", currentItem.id);
                context.startActivity(intent);
            }
        });

        return view;
    }
}
